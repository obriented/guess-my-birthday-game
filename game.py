from random import randint

name = input("Hi what is your name? ")
dayLow = 1
dayHigh = 31
monthLow = 1
monthHigh = 12
yearLow = 1924
yearHigh = 2004
months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]
for guess in range(5):
    month = randint(monthLow, monthHigh)
    year = randint(yearLow, yearHigh)
    if month == (3 or 5 or 8 or 10):
        day = randint(dayLow, dayHigh - 1)
    elif month == (1):
        day = randint(dayLow, dayHigh - 3)
    else:
        day = randint(dayLow, dayHigh)
    print("Guess ", guess + 1, " : ", name, " were you born in ", day, " / ", months[month - 1], " / ", year)
    answer = input("yes, later, earlier? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    if answer == "later":
        yearLow = year
        print("Drat! Lemme try again!")
    if answer == "earlier":
        yearHigh = year
        print("Drat! Lemme try again!")
print("I have other things to do. Good bye.")
